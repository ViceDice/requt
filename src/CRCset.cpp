#ifndef __REQU_CRCSET__
#define __REQU_CRCSET__

#include <malloc.h>
#include "CRCSet.h"
#include <cstring>
#include <cstdio>

CRCSet_t* CRCSet_New()
{
	CRCSet_t* ptr = (CRCSet_t*)malloc( sizeof( CRCSet_t ) );
	memset( ptr, 0, sizeof( CRCSet_t ) );
	return ptr;
}

void CRCSet_Free(CRCSet_t* ptr)
{
	if( ptr )
	{
		free( ptr );
	}
}

void CRCSet_Print( CRCSet_t* ptr )
{
	if( ptr )
	{
		printf( "CRC %u %u %u %u %u %u \n ", ptr->crc1, ptr->crc2, ptr->crc3, ptr->crc4, ptr->crc5, ptr->moda);
	}
}

bool CRCSet_Compare ( CRCSet_t* crc1, const CRCSet_t* crc2 )
{
	return (   ( crc1->crc1 == crc2->crc1 ) && ( crc1->crc2 == crc2->crc2 ) && ( crc1->crc3 == crc2->crc3 )
	 		&& ( crc1->crc4 == crc2->crc4 ) && ( crc1->crc5 == crc2->crc5 ) && ( crc1->moda == crc2->moda ) );
}

#endif // __REQU_CRCSET__