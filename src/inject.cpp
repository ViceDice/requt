#ifndef __REQU_INJECT__
#define __REQU_INJECT__


#include "fonline.h"
#include "requversion.h"

#include "inject.h"
#include "macros.h"
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>

bool is_big_endian()
{
	unsigned short tmp = 0x0001;
	char* ptr = (char*)&tmp;
	return *ptr == 0;
}



extern "C" __attribute__((__stdcall__)) void render_map()
{

	asm ( "push %eax ");
	asm ( "push %ebx ");
	asm ( "push %ecx ");
	asm ( "push %edx ");
	asm ( "push %esi ");
	asm ( "push %edi ");
	asm ( "pushf" );

	InjectConfig_t* cfg = (InjectConfig_t*)0xBAADF00D; // dont touch, dont duplicate, JUST DONT

	void (*ScriptRun)() = ( void(*)() )0x00511510;
	ScriptRun();

	if( cfg->dllData )
	{
		// if( cfg->dllData->LogFn )
		// {
	 // 		void (*logfn)(const char* frmt, ...) = ( void (*)(const char*, ...) )cfg->dllData->LogFn;

	 // 		logfn( " RENDERING FROM HAX ! \n " );
	 			
		// }

		if( cfg->dllData->GameOpt )
		{
		//	GameOptPtrTable_t* gopt = GameOptPtrTable_New();
		//	GameOptPtrTable_From( (char*)cfg->dllData->GameOpt, gopt, cfg->vData->version );

		//	*gopt->ShowCritId = true;
		}
	}

	asm( "popf");
	asm( "pop %edi");
	asm( "pop %esi");
	asm( "pop %edx");
	asm( "pop %ecx");
	asm( "pop %ebx");
	asm( "pop %eax");

	return;
}


#endif // __REQU_INJECT__