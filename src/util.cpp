#ifndef __REQU_UTIL__
#define __REQU_UTIL__

#include "util.h"

bool is_big_endian()
{
	unsigned short tmp = 0x0001;
	char* ptr = (char*)&tmp;
	return *ptr == 0;
}

#endif // __REQU_UTIL__