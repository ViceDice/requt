#ifndef __REQU_OBJECTF__
#define __REQU_OBJECTF__

#include "objectf.h"
#include <cstdlib>
#include <cstring>

ObjFunction_t* ObjFunction_New()
{
	ObjFunction_t* ptr = (ObjFunction_t*)malloc( sizeof( ObjFunction_t )  );
	memset( ptr, 0, sizeof( ObjFunction_t ) );
	return ptr;
}


void ObjFunction_Free( ObjFunction_t* ptr )
{
	if( ptr )
	{
		if( ptr->bytes )
		{
			free( ptr->bytes );
		}

		free( ptr );
		ptr->size = 0;
	}
}



#endif 