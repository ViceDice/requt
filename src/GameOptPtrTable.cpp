#ifndef __REQU_GAMEOPTPTRTABLE__
#define __REQU_GAMEOPTPTRTABLE__


#include "GameOptPtrTable.h"
#include <malloc.h>
#include <cstring>



GameOptPtrTable_t* GameOptPtrTable_New()
{
	GameOptPtrTable_t* ptr = (GameOptPtrTable_t*)malloc( sizeof( GameOptPtrTable_t ) );
	memset( ptr, 0, sizeof( GameOptPtrTable_t ) );
	return ptr;
}

void GameOptPtrTable_Free(GameOptPtrTable_t* ptr)
{
	if( ptr )
	{
		free( ptr );
	}
}





#endif // __REQU_GAMEOPTPTRTABLE__