#ifndef __REQU_PROTOITEMPTRTABLE__
#define __REQU_PROTOITEMPTRTABLE__


#include "ProtoItemPtrTable.h"
#include <malloc.h>
#include <cstring>



ProtoItemPtrTable_t* ProtoItemPtrTable_New()
{
	ProtoItemPtrTable_t* ptr = (ProtoItemPtrTable_t*)malloc( sizeof( ProtoItemPtrTable_t ) );
	memset( ptr, 0, sizeof( ProtoItemPtrTable_t ) );
	return ptr;
}

void ProtoItemPtrTable_Free(ProtoItemPtrTable_t* ptr)
{
	if( ptr )
	{
		free( ptr );
	}
}




#endif // __REQU_PROTOITEMPTRTABLE__