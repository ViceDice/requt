#ifndef __REQU_REQUVERSION__
#define __REQU_REQUVERSION__


#include "requversion.h"
#include <cstring>
#include <cstdlib>
#include "CRC.h"

const Version_t SupportedVersions[] = 
{   
	// D3D.exe
	{VERSION_TYPE_DX, 1, {868402575, 0, 0, 0, 0, 3} },
	{VERSION_TYPE_DX, 2, {1721739964,0, 0, 0, 0, 1} },
	{VERSION_TYPE_DX, 3, {881414099, 0, 0, 0, 0, 0} },
	{VERSION_TYPE_DX, 4, {3966273126,0, 0, 0, 0, 2} },
	{VERSION_TYPE_DX, 5, {2597640453,0, 0, 0, 0, 1} },
	{VERSION_TYPE_DX, 6, {548290865,0, 0, 0, 0, 4} },
	{VERSION_TYPE_DX, 7, {2584917894,0, 0, 0, 0, 3} },
	{VERSION_TYPE_DX, 8, {1258944807,0, 0, 0, 0, 2} },
	{VERSION_TYPE_DX, 9, {3513863347,0, 0, 0, 0, 0} }, // 31.08.2023
	// OGL
	{VERSION_TYPE_OGL, 1, {1161980254,0, 0, 0, 0, 0} },
	{VERSION_TYPE_OGL, 2, {400045374, 0, 0, 0, 0, 2} },
	{VERSION_TYPE_OGL, 3, {3105018328, 0, 0, 0, 0, 4} },
	{VERSION_TYPE_OGL, 4, {1859419085, 0, 0, 0, 0, 3} },
	{VERSION_TYPE_OGL, 5, {3954147662, 0, 0, 0, 0, 1} }, // 31.08.2023
	
};

const unsigned int VersionsCount 		= sizeof( SupportedVersions ) / sizeof( Version_t );

const Version_t* GetVersion( unsigned int id )
{
	const Version_t* result = NULL;
	for( unsigned int i = 0; i < VersionsCount; i++ )
	{
		const Version_t* tmp = &(SupportedVersions[i]);
		if( tmp->id == id )
		{
			result = tmp;
			break;
		}
	}
	return result;
}

const Version_t* CheckFileVersion( FILE* file )
{
	const Version_t* result = NULL;
	if( file )
	{
		fseek(file, 0, SEEK_END);
		long fileLen = ftell(file);
		fseek(file, 0, SEEK_SET);
		if( fileLen )
		{
			fseek( file, 0x05, SEEK_SET );
			char endianness = fgetc( file );
			printf( " ELF file endianness is %s \n" , endianness == 1 ? "Little" : "Big " );
			fseek( file, 0, SEEK_SET );
			

			struct CRCSet* exeCRC = CRCSet_New();
		 	unsigned int chunkSize 	  = fileLen;
		 	unsigned int moda 		  = fileLen%5;

		 	exeCRC->moda = moda;

			for( unsigned int i = 0; i < fileLen/chunkSize; i++ )
			{
				char* buf = (char*)malloc(chunkSize);
				char* read = fgets( buf, chunkSize, file );
				if( !read )
				{
					printf( "Cannot read chunk %d \n ", i );
				}

				int err = ferror( file );
				if( err )
				{
					printf(" eror when read shit from file %d \n", err);
					result = NULL;
					break;
				}

				unsigned int crc = CRC::Calculate( buf, chunkSize, CRC::CRC_32() );

				switch( i )
				{
					case( 0 ):
						exeCRC->crc1 = crc;
						break;
					case( 1 ):
						exeCRC->crc2 = crc;
						break;
					case( 2 ):
						exeCRC->crc3 = crc;
						break;	
					case( 3 ):
						exeCRC->crc4 = crc;
						break;
					case( 4 ):
						exeCRC->crc5 = crc;
						break;
					default:
						break;
				}

				free( buf );
			}
			//

			CRCSet_Print( exeCRC );
			for( unsigned int i = 0; i < VersionsCount; i++ )
			{
			 	if( CRCSet_Compare( exeCRC, &(SupportedVersions[i].crc) ) )
				{
					result = &(SupportedVersions[i]);
				 	break;
				}
			}

			CRCSet_Free( exeCRC );
		}
	}

	return result;
}

VersionData_t* VersionData_New()
{
	VersionData_t* ptr = ( VersionData_t* )malloc( sizeof ( VersionData_t ) );
	memset( ptr, 0, sizeof ( VersionData_t ) ) ;
	return ptr;
}

void VersionData_Init( const Version_t& version, VersionData_t* ptr)
{
	if( ptr )
	{
		ptr->version = version;
		ptr->isInit  = false;
		if( version.type == VERSION_TYPE_DX )
		{
			switch( version.id ) 
			{
				case ( 1 ):
				{
					ptr->off_protoCont  	= 0x1558214;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}

				case ( 2 ):
				{
					ptr->off_protoCont  	= 0x1558214;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}

				case ( 3 ): // major version, not guaranteed to work
				{
					ptr->off_protoCont  	= 0x1524B44;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}

				case ( 4 ): // major version, not guaranteed to work
				{
					ptr->off_protoCont  	= 0x1525B44;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case ( 5 ):
				{
					ptr->off_protoCont  	= 0x1525B4C;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case ( 6 ):	
				{
					ptr->off_protoCont  	= 0x1525B4C;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case ( 7 ):	
				{
					ptr->off_protoCont  	= 0x1526BCC;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case ( 8 ):	
				{
					ptr->off_protoCont  	= 0x1526BCC;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case ( 9 ):	
				{
					ptr->off_protoCont  	= 0x1527BCC;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				


				default:
					break;
			}
		}
		else
		if( version.type == VERSION_TYPE_OGL )
		{
			switch( version.id )
			{
				case( 1 ):
				{
					ptr->off_protoCont  	= 0x15029D4;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					// not calculated for ogl, stubs
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case( 2 ):
				{
					ptr->off_protoCont  	= 0x15039DC;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					// not calculated for ogl, stubs
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case( 3 ):
				{
					ptr->off_protoCont  	= 0x1504A7C;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					// not calculated for ogl, stubs
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case( 4 ):
				{
					ptr->off_protoCont  	= 0x1504A7C;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					// not calculated for ogl, stubs
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				case( 5 ):
				{
					ptr->off_protoCont  	= 0x1505A7C;
					ptr->protoCount 		= 30000;
					ptr->protoSize  		= 908;
					// not calculated for ogl, stubs
					ptr->call_render_map  	= 0x004FB880;
					ptr->displacement_call_render_map = 0x8B5C0100;
					ptr->fn_Script_RunPrepared      = 0x00511510;
					ptr->fn_HexManager_RebuildMap 	= 0x004FAD80;
					ptr->isInit     		= true;
					break;
				}
				
				default:
					break;
			}
			
		}
		
	}
}

void VersionData_Free( VersionData_t* ptr )
{
	if( ptr ) 
	{
		free ( ptr );
	}
}


void GameOptPtrTable_From( char* addr, GameOptPtrTable_t* table, const Version_t& version )
{
	if( table && addr )
	{
		table->version = version;
		table->isInit   = false;
		if( version.type == VERSION_TYPE_DX )
		{
			switch ( version.id )
			{
				case ( 1 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}

				case ( 2 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}

				case ( 3 ): // not sure if working
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}

				case ( 4 ): // not sure if working
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}

				case ( 5 ): // not sure if working
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case ( 6 ): 
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case ( 7 ): 
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case ( 8 ): 
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case ( 9 ): 
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}

				default:
					break;
			}
		}
		else
		if( version.type == VERSION_TYPE_OGL )
		{
			switch( version.id )
			{
				case( 1 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case( 2 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case( 3 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case( 4 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552);
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				case( 5 ):
				{
					table->YearStart 	 		= (uint16*)(addr);
					table->YearStartFTLo 		= (uint*)(addr+4);
					table->YearStartFTHi 		= (uint*)(addr+8);
					table->Year 				= (uint16*)(addr+12);
					table->Month   	 			= (uint16*)(addr+14);
					table->Day 					= (uint16*)(addr+16);
					table->Hour					= (uint16*)(addr+18);
					table->Minute 				= (uint16*)(addr+20);
					table->Second     			= (uint16*)(addr+22);
					table->FullSecond 			= (unsigned int*)(addr+26);
					table->Quit  	  			= (bool*)(addr+552); // not sure
					table->TimeMultiplier 		= (uint*)(addr+32);
					table->Light 				= (uint*)(addr+616);
					table->SpritesZoom 			= (float*)(addr+956);
					table->SpritesZoomMax		= (float*)(addr+960);
					table->SpritesZoomMin		= (float*)(addr+964);
					table->ShowTile				= (bool*)(addr+580);
					table->ShowRoof				= (bool*)(addr+581);
					table->ShowItem				= (bool*)(addr+582);
					table->ShowScen 			= (bool*)(addr+583);
					table->ShowWall				= (bool*)(addr+584);
					table->ShowCrit				= (bool*)(addr+585);
					table->ShowFast				= (bool*)(addr+586);
					table->ShowPlayerNames		= (bool*)(addr+587);
					table->ShowNpcNames			= (bool*)(addr+588);
					table->ShowCritId			= (bool*)(addr+589);
					table->isInit     			= true;
					break;
				}
				
				default:
					break;
			}
		}
		

	}
}


void ProtoItemPtrTable_From( char* addr, ProtoItemPtrTable_t* table, const Version_t& version )
{
	if( table && addr)
	{
		table->version = version;
		table->isInit   = false;
		if( version.type == VERSION_TYPE_DX )
		{
			switch ( version.id )
			{
				case ( 1 ):
				{
					table->pid   			= (unsigned short*)addr;
					table->type  			= (unsigned int*)(addr+4);
					table->flags 			= (unsigned int*)(addr+16);
					table->weight			= (unsigned int*)(addr+36);
					table->lightColor   	= (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   	= (bool*)(addr+64);
					table->isInit 			= true;
					break;
				}

				case ( 2 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   	= (bool*)(addr+64);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}

				case ( 3 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   	= (bool*)(addr+64);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}

				case ( 4 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case ( 5 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case ( 6 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case ( 7 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case ( 8 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case ( 9 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}

				default:
					break;
			}
		}
		else
		if( version.type == VERSION_TYPE_OGL )
		{
			switch( version.id )
			{
				case( 1 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				
				case( 2 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case( 3 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case( 4 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				case( 5 ):
				{
					table->pid   		= (unsigned short*)addr;
					table->type  		= (unsigned int*)(addr+4);
					table->flags 		= (unsigned int*)(addr+16);
					table->weight		= (unsigned int*)(addr+36);
					table->lightColor   = (unsigned int*)(addr+60);
					table->lightIntensity 	= (char*)(addr+56);
					table->lightDistance	= (unsigned char*)(addr+55);
					table->lightFlags	 	= (unsigned char*)(addr+54);
					table->disableEgg   = (bool*)(addr+64);
					table->isInit 		= true;
					break;
				}
				
				default:
					break;
			}
		}
		
		
	}
}




#endif // __REQU_REQUVERSION__