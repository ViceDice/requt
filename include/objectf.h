#ifndef __REQU_OBJECTF_H__
#define __REQU_OBJECTF_H__


struct ObjFunction
{
	char* bytes;
	unsigned int base;
	unsigned int size;
};

typedef ObjFunction ObjFunction_t;

ObjFunction_t* ObjFunction_New();
void ObjFunction_Free( ObjFunction_t* ptr );

#endif 