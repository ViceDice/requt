#ifndef __REQU_DLLDATA_H__
#define __REQU_DLLDATA_H__

#include "GameOptPtrTable.h"

struct DllData
{
	void* GameOpt;
	void* LogFn;
	void* ASEngine;
};

#endif