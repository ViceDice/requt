#ifndef __REQU_PROTOITEMPTRTABLE_H__
#define __REQU_PROTOITEMPTRTABLE_H__


#include "macros.h"
#include "version.h"

struct ProtoItemPtrTable
{
	Version_t version;
	bool isInit;
	
	unsigned short* pid;
	unsigned int*	type;
	unsigned int* 	flags;
	unsigned int* 	weight;
	unsigned char*  lightFlags;
	unsigned char*  lightDistance; 
    char*			lightIntensity;
	unsigned int*   lightColor;
	bool* 			disableEgg;
	
};

typedef struct ProtoItemPtrTable ProtoItemPtrTable_t;

ProtoItemPtrTable_t* ProtoItemPtrTable_New();
void ProtoItemPtrTable_Free(ProtoItemPtrTable_t* ptr);




#endif // __REQU_PROTOITEMPTRTABLE_H__