#ifndef __REQU_FONLINE_H__
#define __REQU_FONLINE_H__

#include "macros.h"

// Item types
#define ITEM_TYPE_BEGIN							 (0)
#define ITEM_TYPE_NONE                           (0)
#define ITEM_TYPE_ARMOR                          (1)
#define ITEM_TYPE_DRUG                           (2)
#define ITEM_TYPE_WEAPON                         (3)
#define ITEM_TYPE_AMMO                           (4)
#define ITEM_TYPE_MISC                           (5)
#define ITEM_TYPE_KEY                            (7)
#define ITEM_TYPE_CONTAINER                      (8)
#define ITEM_TYPE_DOOR                           (9)
#define ITEM_TYPE_GRID                           (10)
#define ITEM_TYPE_GENERIC                        (11)
#define ITEM_TYPE_WALL                           (12)
#define ITEM_TYPE_CAR                            (13)


struct SpriteInfo
{
    const void*  Surface;
    const float  SurfaceUV[ 4 ];
    const uint16 Width;
    const uint16 Height;
    const int16  OffsX;
    const int16  OffsY;
    const void*  Effect;
    const void*  Anim3d;     // If Anim3d != NULL than this is pure 3d animation
};

struct Sprite
{
    // Ordering
    const int     DrawOrderType;     //. 0.4 - flat, 5..9 - normal
    const uint    DrawOrderPos;
    const uint    TreeIndex;

    // Sprite information, pass to GetSpriteInfo
    const uint    SprId;
    const uint*   PSprId;     // If PSprId == NULL than used SprId

    // Positions
    const int     HexX, HexY;
    const int     ScrX, ScrY;
    const int16*  OffsX, * OffsY;

    // Cutting
    const int     CutType;     // See Sprites cutting
    const Sprite* Parent, * Child;
    const float   CutX, CutW, CutTexL, CutTexR;

    // Other
    const uint8*  Alpha;
    const uint8*  Light;
    const int     EggType;
    const int     ContourType;
    const uint    ContourColor;
    const uint    Color;
    const uint    FlashMask;
    const bool*   ValidCallback;
    const bool    Valid;     // If Valid == false than this sprite not valid

    #ifdef __MAPPER
    const int     CutOyL, CutOyR;
    #endif

    uint const GetSprId()
    {
        return PSprId ? *PSprId : SprId;
    }

  // SpriteInfo* const GetSprInfo()
  //  {
  //      return FOnline->GetSpriteInfo( PSprId ? *PSprId : SprId );
  //  }

    // void const GetPos( int& x, int& y )
    // {
    //     SpriteInfo* si = GetSprInfo();
    //     x = (int) ( (float) ( ScrX - si->Width / 2 + si->OffsX + ( OffsX ? *OffsX : 0 ) + FOnline->ScrOx ) / FOnline->SpritesZoom );
    //     y = (int) ( (float) ( ScrY - si->Height    + si->OffsY + ( OffsY ? *OffsY : 0 ) + FOnline->ScrOy ) / FOnline->SpritesZoom );
    // }
};


#endif