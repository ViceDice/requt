#ifndef __REQU_VERSION_H__
#define __REQU_VERSION_H__

#define VERSION_TYPE_DX  0
#define VERSION_TYPE_OGL 1
#define VERSION_TYPE_UNKNOWN 2

#include "CRCSet.h"

struct Version
{
	unsigned int type;
	unsigned int id;
	CRCSet_t crc;
};

typedef struct Version Version_t;

#endif // __REQU_VERSION_H__