#ifndef __REQU_WINUTILS_H__
#define __REQU_WINUTILS_H__

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
//#define _WIN32_WINNT 0x0600
// Windows Header Files:

#include <Windows.h>
#include <tlHelp32.h>
#include <tchar.h>
#include <stdio.h>
#include <psapi.h>
#include <commctrl.h>
#include <processthreadsapi.h>

unsigned int GetProcessHandles ( const char* procName, HANDLE*& ptr, unsigned int size );
unsigned int GetProcId 	   ( HANDLE procHandle );
void CloseProcessHandles( HANDLE* ptr, unsigned int size );
uintptr_t GetModuleBaseAddressName( HANDLE procHandle, char* modName);
HMODULE WINAPI GetRemoteModuleHandle(HANDLE hProcess, LPCSTR lpModuleName);
FARPROC WINAPI GetRemoteProcAddress (HANDLE hProcess, HMODULE hModule, LPCSTR lpProcName, UINT Ordinal = 0, BOOL UseOrdinal = FALSE);
bool ReadRemoteProcessMemory( HANDLE hProcess, void* offset, void* buffer, unsigned int buflen, unsigned int* bytesRead );
bool WriteRemoteProcessMemory( HANDLE hProcess, void* offset, void* buffer, unsigned int buflen, unsigned int* bytesWrite );
unsigned int GetProcessThreads( DWORD procId, unsigned int*& thread_ids_ptr, unsigned int size );
bool SuspendProcess( HANDLE procHandle );
bool ResumeProcess(  HANDLE procHandle );


#endif // __REQU_WINUTILS_H__