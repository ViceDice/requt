#ifndef __REQU_CRCSET_H__
#define __REQU_CRCSET_H__

#include "macros.h"


struct CRCSet
{
	unsigned int crc1;
	unsigned int crc2;
	unsigned int crc3;
	unsigned int crc4;
	unsigned int crc5;
	unsigned int moda;
};

typedef struct CRCSet CRCSet_t;

CRCSet_t* CRCSet_New();
void CRCSet_Free(CRCSet_t* ptr);
void CRCSet_Print( CRCSet_t* ptr );
bool CRCSet_Compare ( CRCSet_t* crc1, const CRCSet_t* crc2 );

#endif // __REQU_CRCSET_H__