#ifndef __REQU_MACRO_H__
#define __REQU_MACRO_H__

typedef char                                  int8;
typedef unsigned char                         uint8;
typedef short                                 int16;
typedef unsigned short                        uint16;
typedef unsigned int                          uint;

// #define DEFAULT_NEW_MALLOC(type) \
// type#* type#_New()			 	 \
// {								 \
// 	type#* ptr = (type#*)malloc( sizeof( type ) ); \
// 	memset( ptr, 0, sizeof( type ) ); \
// 	return ptr;						  \
// }			

#endif