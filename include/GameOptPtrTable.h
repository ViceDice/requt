#ifndef __REQU_GAMEOPTPTRTABLE__H__
#define __REQU_GAMEOPTPTRTABLE__H__


#include "macros.h"
#include "version.h"

struct GameOptPtrTable
{
	Version_t version;
	bool isInit;

	uint16*       YearStart;
    uint*         YearStartFTLo;
    uint*         YearStartFTHi;
    uint16*       Year;
    uint16*       Month;
    uint16*       Day;
    uint16*       Hour;
    uint16*       Minute;
    uint16*       Second;

    uint* Light;
	bool* Quit;
	uint* FullSecond;
	uint* TimeMultiplier;
	float* SpritesZoom;
	float* SpritesZoomMax;
	float* SpritesZoomMin;
	bool* ShowTile;
	bool* ShowRoof;
	bool* ShowItem;
	bool* ShowScen;
	bool* ShowWall;
	bool* ShowCrit;
	bool* ShowFast;
	bool* ShowNpcNames;
	bool* ShowPlayerNames;
	bool* ShowCritId;
};

typedef struct GameOptPtrTable GameOptPtrTable_t;

GameOptPtrTable_t* GameOptPtrTable_New();
void GameOptPtrTable_Free(GameOptPtrTable_t* ptr);


#endif // __REQU_GAMEOPTPTRTABLE__H__