#ifndef __REQU_INJECT_H__
#define __REQU_INJECT_H__

#include "requversion.h"
#include "dlldata.h"

struct InjectConfig
{
	struct VersionData*  vData;
	struct DllData* 	 dllData;

};

typedef InjectConfig InjectConfig_t;

#endif // __REQU_INJECT_H__