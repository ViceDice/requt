#ifndef __REQU_REQUVERSION_H__
#define __REQU_REQUVERSION_H__

#include <cstdio>
#include "CRCSet.h"
#include "ProtoItemPtrTable.h"
#include "GameOptPtrTable.h"
#include "version.h"




struct VersionData
{
	Version_t    version;
	bool 		 isInit;
	// PROTOTYPES
	unsigned int off_protoCont   ;
	unsigned int protoCount 	 ;
	unsigned int protoSize  	 ;

	//various Offsets
	unsigned int call_render_map;
	unsigned int displacement_call_render_map;
	unsigned int fn_Script_RunPrepared;
	unsigned int fn_HexManager_RebuildMap;
};

typedef struct VersionData VersionData_t;

const Version_t* CheckFileVersion( FILE* file );
bool ValidVersion ( const Version_t& version );
const Version_t* GetVersion( unsigned int id );
VersionData_t* VersionData_New();
void VersionData_Init( const Version_t& version, VersionData_t* ptr);
void VersionData_Free( VersionData_t* ptr );
void GameOptPtrTable_From( char* addr, GameOptPtrTable_t* table, const Version_t& version );
void ProtoItemPtrTable_From( char* addr, ProtoItemPtrTable_t* table, const Version_t& version );


#endif